<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		$vista = array();
		if (isset($_POST['validar'])) {
			$idUsuario= $this->input->post('user'); 
			$passwor= $this->input->post('pass');
			$this->load->model("M_Usuario");
			$fila = $this->M_Usuario->verificar($idUsuario,$passwor);
			if ($fila!= null) {
				header("location:".base_url()."create");
			}else{
				$vista ["mensaje"] = 'error';
				$vista ["texto"] = 'El usurio o su contraseña no son correcto!';
			}
		}
		$this->load->view('login',$vista);
	}

	public function create(){
		$vista = array();
		if (isset($_POST['guardar'])) {
			$data = array();
			$data ['nombre']=$_POST['nombre'];
			$data ['apellido']=$_POST['apellido'];
			$data ['telefono']=$_POST['telefono'];
			$data ['contrasena']=$_POST['contrasena'];
			$data ['estado']= 1;

			$this->load->model("M_Usuario");
			if ($this->M_Usuario->insertar($data)>0) {
				$vista ["mensaje"] = 'success';
				$vista ["texto"] = 'Usuario guardado.!';
			}else{
				$vista ["mensaje"] = 'error';
				$vista ["texto"] = 'El Usuario no se pudo guardar!';
			}
		}

		if (isset($_POST['modificar'])) {
			$data = array();
			$data ['nombre']=$_POST['nombre'];
			$data ['apellido']=$_POST['apellido'];
			$data ['telefono']=$_POST['telefono'];
			$data ['contrasena']=$_POST['contrasena'];
			$this->load->model("M_Usuario");
			if ($this->M_Usuario->Actualizar($_POST['id'],$data)>0) {
				$vista ["mensaje"] = 'success';
				$vista ["texto"] = 'Usuario modificado.!';
			}else{
				$vista ["mensaje"] = 'error';
				$vista ["texto"] = 'El Usuario no se pudo modificar.!';
			}
		}

		if (isset($_POST['eliminar'])) {
			$data = array();
			$data ['estado']= 0;
			$this->load->model("M_Usuario");
			if ($this->M_Usuario->Actualizar($_POST['eliminar'],$data)>0) {
				$vista ["mensaje"] = 'success';
				$vista ["texto"] = 'Usuario Eliminado!';
			}else{
				$vista ["mensaje"] = 'error';
				$vista ["texto"] = 'El Usuario no se pudo eliminar.!';
			}
		}

		$this->load->model("M_Usuario");

		$vista ["usuarios"] = $this->M_Usuario->listar();
		$this->load->view('registro',$vista);


	}
}
