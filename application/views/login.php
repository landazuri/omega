<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>OMEGA</title>
	<link href="<?= base_url()?>plantilla/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?= base_url()?>plantilla/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="<?= base_url()?>plantilla/css/animate.css" rel="stylesheet">
	<link href="<?= base_url()?>plantilla/css/style.css" rel="stylesheet">
	<!-- Sweet Alert -->
	<link href="<?= base_url()?>plantilla/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
</head>
<body class="gray-bg">
	<div class="middle-box text-center animated fadeInDown" style="margin-top: 90px;">
		<div>
			<div >
				<img alt="image" class="rounded-circle circle-border" width="150"  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABVlBMVEX///8OFhkAAAAOFRtHR0dISknd3t0NFxkNFhv4+vpNTU0OFRh+goX9//8KExZMTk2dGh/UIijw8PDo6uoSHSDS0tLIyMi/v7+1tbXSISfYIiRYWFiqHSWcGSFRU1L///yjo6NBREJwcHBmamwnKy31///KHycACw8fIyQAAAi7HiV7e3unp6c+Pj6bm5tgYGCzHCWNj451dnaVAAAzMjOIiYj59fH15N7ap6TPcXHAOD6+ABq2AACzOkDFdnfhw7vKX2THABSvDheiABa3ZGnvycfOT1XBHymvVlrVjY/JmZgxNjrTABGbAAqpa2/HJyKsREvdh5Ds39nn0sqhRUjYAArp8+y4SE6nND+RIBzZvr3EAAecIhakAADGb3iGAADqT1fmeHvCjYmIHyfRN0CfV1jiIyPHoqGjJS/sqrHYzsTmuru+Zl7WNTnXr7v+7/SlZ3Hv2+FcuvLIAAAOkklEQVR4nO2b6X/bxpnHwSHAAxAAmQhviZcEQRQJUqQl+lBqh1R8xJbLWooTu66a1O3a2XqzWv//b/aZGRwzIOkwdHff9Pn687FFEMfzm3muGciKgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiDIvxlmsVyUj+QDEic6pXIpk7yan2XKJ8O15sJ5efnTwhmhOQvPXXa730PRbqvthmh58aTtU3rSeaWhr6qqb7ckAyonJyX4p3xyUo4PFlS4QXvYF8/sn5wUxct8esZoYcAUc+iLt2JQY9p2p78gfS0c3bY1u+GX4kNFtdGwbV3dEs8btW0bFOoNre4IhyuqyhSqqqiwTm9gt3vyiYKcikZPsG1/lLSnCN90Esdsdjtb1YvJs9ehotqDzMgfCo8HhZ1ToC+cNmg3tF65WB417PapbDhVWFI1SaE9PD1tSMNWUX1Jod0bbQ3bDXWQsGcEctqOfMxu1EenY1vX9cQXa9FiY9sXLy2qWiVxFojWuLlOR3LeFQp1OKnY1gTz+1pbUkg/5csgUXbUQgMUqn3pmKKzWXXGui4HznqUfH2YGJklCnt65IUFOQ5XKyyoosJKUmGRX67JflpWtdZQr8tP13Xmt/mhri4G7m8z1vSGfF3gpVvxSBZUPRkboakrFHaKpZ6tCgO1XKFp23vSiI11tTBQVTngAoX0YcmhX4fCUNXFgGEKbZppYpfI+HoyXkJTlyts6Kpq242CcOJShUpPOklxfH1Lyai6GOqxwtVmfJ58R2v44tjQXApRLeTSjK+16L9lvU4ZxsO+UiHcoiMl3eUKt+TsMdDacLthoyEFTqgQ9C/k3vUYqQ1f8AuIw0SoF3w+qhUfJrehDwVTV3jpeKhLk7NK4Vg6zWzYQ6dQqGhyIvjSOVRoQhWzVHHR3Ye6Tg0pdXo9yNrCuaHCdkLhaVmVx3u5wryqi3FYakMi9f1kSQwV9jU12QysAzRiplLXBc9borAPQxCcMLDFx5S5woqmCqHMcikMipjAliscaVJB7Om2RrHlIhIodDYqiPktf6+g5HXR88BLW04mkxHuBjVc69CHFgaaLZxKs8I4rxQ6uljPmcKSL+WLpEKt7GSKp6pkMwTDsEUZ6GKhUWy7A+ZU6rq6QRhCjbHrPbhWuCPNpTCQUteW0SAC93rjumb7Um2B64fjujRCQT3s6OpCjY8/QbrWVFv3RberRL0flETBeVnXpmnwECm018Rp0NqgitcWfeopqio1EJlhGx6h26otF8+MqtHKIrlPQVfH9Db075B+W+ra2swb27pUphqqHpjR90Xp/GTN720iEGZxAEEote3OgLlKKxGMpdO63hhXkg1+Aa6vD6Rn5/vs2kprEOsuih/gE9CvyIUdnhs+sjAQn86tKW/SlCIIgiDI76HZNJW86brN5mQygZ9NN6+YX7KFtzaffci/zgSX3qx55+69+2dfA394cPeO4v5fCjQLxQot+iVnsy3CDZ7oNh9+M52ezedHwGw2O//2UdNNnOOMt4Bxclunx47GlvLTRJKXZEYqCTnsVFY2Y/zWvY02EUXD6R/z0eO30/kTgCo8BmbXTx+xb2M/qnCbEqvQLX40NiNDFpB62eKQEM8wcilKziIku7WiIdvjV9eXf7s+rmLmv5vWDoBY4M7OzvmLZyYNhGgqByQNNnnP5cuJRS0lcR+ZIakEJFLQhPUa6Rr0YDoglbKIkdhU4BRIig6D1f3SjtQ1J48v3s8XFO4cv/jjRIz1vW6a2SvNSInrIfHS7XMKFcdf+JaKIHtL4rFPsmyiwWu+KCeYyuR+bT6vygqZxDezpxMhnx1a6eCBAj1usRe70ucUZjw+SqmUkbWAbDYLP2dzBtEXJe5bTKHh+V8iD2Kw+fji4BYTCMzOgOMjJnBnZ/vFt3FONUmKWSc/0LP4LFhJhR7pBpBIYQFcOgdmpz2aY/b3aZQZIDCXTpOTpMQifMXm0CCb5xoqUPnu4la1elA9OJrPp7P79x789U9n58dHTOD29u1n0RxmSJaPv+imgZOCFVFG5Aqt+l5E3eH3yD/3IASz2TQ5HBTZIacyJN0sjJxBeklP3ApuncuSsbIptA5+rNVqTOG8dvTyE5uxywfn54HCndtX4ZMrwRxKbtoLXTIeZ65QitZglJjN2axHBoKazJBYWYNIb7rYcHStUKF1uNnqnj5ZcSf339dgDsFLp3/45LrK5CYPyfXq9TlXuHv+fWjNKFToqbEZh1aoMEqmTOEyzyrytOsdJt4+VIhHThfOrnBXgEtA/ya7+ZymefdV7dYtCMPq/P4PinJ17/H9H+99MhXn9fnxLijcfnP7zyZPZQ0vynyRiSWSDhVGNq5UOOzStOLtLyT/4qJAkz/Oeg7phuaxTZMpzOHj91ThwUH17FJxX76dHhzNzmfPFOXP4KhM4fb3gZNZ4XQJbjqOFHoNSWFqUSEczy79Ar5acohNODltkSwdr01eOjGF5tVFjSu8+M5VXr69gK7t+Ogv189M969c4e7uT5cuncQCMVJJNYV9K1Ro7UsKl9g0YgrJmnueoyCaHa50wxcWkGpehgqnl8rlq9qTg4OjneOj7d1L5Wq2zRS+uf2sSVMSz5q8OITml4iRDo5ZXuh8fA67/VIMKwS6B8Vv3aSRP6FO6tmmUmfeeripwubPNaZwXnucd/82hXh8AgUf6uA7pfn6nM/h7lPabfHI9xq+JbjpGI5lvfq+RSct3PvkCg2xL6XiHRiIdIqs+Rq3RD3GINDNlent0mSTNxaU/C2msFad3lOUf7yvhgrP/6gofwoV/p0pZKme9PpcKIvMwj6TW6ZJIc53K3qaDNgMCte0dK9LZy5LL6RVI93d21Dhzav3bA6rtXtmrPB4+/w/FPNpqPDNhCpUPTZ5Do8y5pLMca1Ds9Olh8KKtkIhPTmd6q6XMhzm+YS9kRnRoUmTDUviI66wCgpd5ef3gZce78IcmtEc3r6kC6xDPl9K24vclJV7aDhoVjCi9BMqzMkKqZenrZRQKvJOglhEi48jc3wHsmnaSC7a1uUqVFj9x8R8+Yo23zwOPyiTX44DhS8uoVxkWIsBKaZFjGzO08yw3IMZNFTS3nNTVJizYlYoLJOvJLyorzGZw4R31D0YLOurDRVehAqnV+ZkWptzhed/n7gPg1xKywUoDDyyAEMKCtm08EOeyTSlLcMRFGat/Ri2wGPDYHmCl5aJJUGi96JFVpnCaauworth+30nVnhPcf/zbe0JU/gT9DFPw3q4+9MdUNinD/UaeVPxPaqwRZ3UYE6qFGAyIVKKokL4ZAYo+WaTH05nifC2qZwI2DjPbtE7W2Ho5dmKq7tZ+z2JFB5MH7nmx7MLWDxdv3lkKh9nb0IvfdMEKzt0ZQ5PoVohxGAJxVpjHiuaB2v1MJlGCmVo8shJpXtBYSv4Is+qvFcvBPE5ZomVbLRplf85UnjryZXpTu5+882DDxNF+efu8XY4h9C2NcNUSk3lLuRATwptP6G3gdU/TM+WoHCJU8EdIID9uMOMvZSun2CwwkrC3TLlheW0ywdgo/bbfBAohM774OxjsM5x3Q+z2c5OMIfb7+AoK32Bj2lMbH+LKuSeNSCgsFv/vMJT6Npyop1l75Czz3YPjGihrHpMoWFIUywsaX4H7sNXocKDefXr/3p4o5juDw+/uT4OBILC21cur9dhs8lSuaWCWbnAGUtUoefnP6uwSFNU1tuPakLeKQQwhdEWl8N6QbqS5AQK05u03+7ND7++r8ICuMq3aaZv/wld9n9/fc73aXaYk/5C++4SyeVy1vNCYEGKr9zgL5OLgjgMO9NVCk2W9Q2yZ9LEIzLg1S/ccIPJhmUvKDNSdFgp8AlGc3ER+ZuAT373ignk+zTz4wksqG6uj8N9Glg9QWlkRtDxD/ywHq4UU0HsFQ7prmAgapVCiDu+ddZJCCzytW6USWg/k8smgccvbOWsgavc+fX9/FagsHp2D5K7q/wY7URt77z5hd12r5uN90taURIMdbDICSZh1epJUWyWMnLEltbARYOvV8KmhbfaaXlP2TLS2bWbWgHzxlXuXtBdDDaF1dldtwlu+uF6J5pDusRXaHahCgM/ckKF1n7wG9h73Vw0oYurpzIbpGawqoVSQ0ZxMJ4Siy39n4e/zF2n45DuRkFKcfa9dDa3WfttNh9Pw0CcHzgsl366nnGB0L79D+26FQdyQVzTwU0NtpUZddst1g8M2SJkyeopnM8KMXJsG5uQxqBSLJZbQ0IbgShlwQ2CJe9YkXz5lGU3stHutzv5NVR49mOg+nWg8C8vvr8x6ZPA6jT0ZaGlrDcVg40t52AaYoVSKY8Mg3VC0JAHxa5LswnksLiIDMIwlhRmpD7ud2LeOahxhdMPfM/JfDfjgfji9SdeIeXemj4v8CzZAOe3FIL9YZYSS4EVtTOK6bOmu52XFJq84/DUTfoaaMkun0yZxLeXfM/JfXR9HArk0DVaTti5b3g5HniB5vxXVjTwn38zUz4k4T4oh8as0AYUoY9gDZwpK+yv2sFbjzuPX82fVA9+aQYbh7Bygop//e1NuOG91zWMnLBr24ICaFjC83wPqhbPRDCfEpA8pd8J24LMyN45MQyjS+pC3h2DH8frFOEySKdw7sLG8Zo03btn0/n0pRkodB+cza53PyhuM9iOP2FBI75Bo+zHPtNhB3rxd6vfHyrO6BCOeeCiFo3GofjLbQ6/YEnSHPJvNlzquzfmnXfTrx8GO6NN9+Pt3Xc3wsvR4L8JRY7TTPy/IVP8j0T5BZJNTL7Y6mmHlnVSHyXfAAcXLJhomstvtbZEuMPkoxJogkb7I/Sn9N2wvNfclH+IvzTN+O8lbGqWQHjvL7mVKb2H/H/5RQwEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQb6Q/wUoyq0hhYa26AAAAABJRU5ErkJggg==">
			</div>
			<p>REGISTRO DE USUARIOS</p>
			<form role="form" action="" method="POST">
				<div class="form-group row"><label class="col-lg-3 col-form-label">Usuario</label>
					<div class="col-lg-9"><input type="texto" name="user" id="user"  class="form-control" required=""></div>
				</div>
				<div class="form-group row"><label class="col-lg-3 col-form-label">Contraseña</label>
					<div class="col-lg-9"><input type="password" name="pass" id="user" class="form-control" required=""></div>
				</div>
				<button type="submit" name="validar" class="btn btn-primary block full-width">INGRESAR</button>
			</form>
		</div>
	</div>
	<!-- Mainly scripts -->
	<script src="<?= base_url()?>plantilla/js/jquery-3.1.1.min.js"></script>
	<script src="<?= base_url()?>plantilla/js/popper.min.js"></script>
	<script src="<?= base_url()?>plantilla/js/bootstrap.js"></script>
	<!-- Sweet alert -->
	<script src="<?= base_url()?>plantilla/js/plugins/sweetalert/sweetalert.min.js"></script>
	<?php 
	if (@$mensaje == 'success') {
		echo '
		<script>
		swal({
			title: "¡Buen trabajo!",
			text: "'.$texto.'",
			type: "success"
			});
			</script>';
		}elseif (@$mensaje == 'error') {
			echo '
			<script>
			swal({
				title: "¡ALGO PASO!",
				text: "'.$texto.'",
				type: "error"
				});
				</script>';
			}
			?>
		</body>
		</html>