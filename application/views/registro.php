<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>OMEGA</title>
	<link href="<?= base_url()?>plantilla/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?= base_url()?>plantilla/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="<?= base_url()?>plantilla/css/animate.css" rel="stylesheet">
	<link href="<?= base_url()?>plantilla/css/style.css" rel="stylesheet">
	<link href="<?= base_url()?>plantilla/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
	<!-- Sweet Alert -->
	<link href="<?= base_url()?>plantilla/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
</head>
<body class="top-navigation">
	<div id="modal-form" class="modal fade" style="display: none;" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<div class="row">
						<form role="form" method="POST" action="">
							<div class="col-sm-12"><h3 class="m-t-none m-b">REGISTRO DE USUARIO</h3>
								<p>DATOS BASICOS</p>
								<div class="form-group">
									<label>Nombre</label>
									<input type="text" name="nombre" class="form-control" required="">
								</div>
								<div class="form-group">
									<label>Apellido</label>
									<input type="text" name="apellido" class="form-control" required="">
								</div>
								<div class="form-group">
									<label>Telefono</label>
									<input type="number" name="telefono" class="form-control" required="">
								</div>
								<div class="form-group">
									<label>Contraseña</label>
									<input type="text" name="contrasena" class="form-control" required="">
								</div>
							</div>
						</div>
						<div class="text-center">
							<button  type="submit"  name="guardar" class="btn btn-block btn-primary" >GUARDAR</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div id="wrapper">
		<div id="page-wrapper" class="gray-bg">
			<div class="wrapper wrapper-content">
				<div class="container">
					<div class="row">
						<div class="col-lg-2">
							<div class="text-center">
								<a class="btn btn-block btn-primary" href="http://localhost/omega">regresar</a>
							</div>
						</div>
						<div class="col-lg-8"></div>
						<div class="col-lg-2">
							<div class="text-center">
								<a data-toggle="modal" class="btn btn-block btn-primary" href="#modal-form">nuevo usuario</a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox ">
								<div class="ibox-title">
									<h5>Lista de usuarios </h5>
								</div>
								<div class="ibox-content">
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover dataTables-example" >
											<thead>
												<tr>
													<th>Nombre</th>
													<th>Apellido</th>
													<th>Telefono</th>
													<th>Contraseña</th>
													<th>Acciones</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($usuarios as $dt) { ?>
													<tr>
														<td><?= $dt->nombre; ?></td>
														<td><?= $dt->apellido; ?></td>
														<td><?= $dt->telefono; ?></td>
														<td><?= $dt->contrasena; ?></td>
														<form method="POST" action="">
															<td>
																<center>
																	<button class="btn btn-warning btn-xs" data-toggle="modal" type="button" href="#modal<?= $dt->idusuarios; ?>">
																		<i class="fa fa-pencil"></i>
																		<span class="bold"></span>
																	</button>
																	<button class="btn btn-danger btn-xs" type="submit" name="eliminar" value="<?= $dt->idusuarios; ?>" >
																		<i class="fa fa-warning"></i>
																		<span class="bold"></span>
																	</button>
																</center>
															</td>
														</form>
													</tr>
													<div id="modal<?= $dt->idusuarios; ?>" class="modal fade" style="display: none;" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-body">
																	<div class="row">
																		<form role="form" method="POST" action="">
																			<input type="hidden"  name="id" value="<?= $dt->idusuarios; ?>" class="form-control">
																			<div class="col-sm-12"><h3 class="m-t-none m-b">ACTUALIZAR USUARIO</h3>
																				<p>DATOS BASICOS</p>
																				<div class="form-group">
																					<label>Nombre</label>
																					<input type="text" name="nombre" value="<?= $dt->nombre; ?>" class="form-control" required="" >
																				</div>
																				<div class="form-group">
																					<label>Apellido</label>
																					<input type="text" name="apellido" value="<?= $dt->apellido; ?>" class="form-control" required="">
																				</div>
																				<div class="form-group">
																					<label>Telefono</label>
																					<input type="number" name="telefono" value="<?= $dt->telefono; ?>" class="form-control" required="">
																				</div>
																				<div class="form-group">
																					<label>Contraseña</label>
																					<input type="text" name="contrasena" value="<?= $dt->contrasena; ?>" class="form-control" required="">
																				</div>
																			</div>
																		</div>
																		<div class="text-center">
																			<button type="submit"  name="modificar" class="btn btn-block btn-primary" >ACTUALIZAR</button>
																		</div>
																	</form>
																</div>
															</div>
														</div>
													</div>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer">
				<div>
					<strong>Copyright</strong> OMEGA&copy; 2018-2019
				</div>
			</div>
		</div>
	</div>
	<!-- Mainly scripts -->
	<script src="<?= base_url()?>plantilla/js/jquery-3.1.1.min.js"></script>
	<script src="<?= base_url()?>plantilla/js/popper.min.js"></script>
	<script src="<?= base_url()?>plantilla/js/bootstrap.js"></script>
	<script src="<?= base_url()?>plantilla/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="<?= base_url()?>plantilla/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

	<!-- Custom and plugin javascript -->
	<script src="<?= base_url()?>plantilla/js/inspinia.js"></script>
	<script src="<?= base_url()?>plantilla/js/plugins/pace/pace.min.js"></script>

	<!-- Flot -->
	<script src="<?= base_url()?>plantilla/js/plugins/flot/jquery.flot.js"></script>
	<script src="<?= base_url()?>plantilla/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
	<script src="<?= base_url()?>plantilla/js/plugins/flot/jquery.flot.resize.js"></script>

	<!-- ChartJS-->
	<script src="<?= base_url()?>plantilla/js/plugins/chartJs/Chart.min.js"></script>

	<!-- Peity -->
	<script src="<?= base_url()?>plantilla/js/plugins/peity/jquery.peity.min.js"></script>
	<!-- Peity demo -->
	<script src="<?= base_url()?>plantilla/js/demo/peity-demo.js"></script>

	<script src="<?= base_url()?>plantilla/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?= base_url()?>plantilla/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
	<!-- Sweet alert -->
	<script src="<?= base_url()?>plantilla/js/plugins/sweetalert/sweetalert.min.js"></script>

	<!-- Page-Level Scripts -->
	<script>
		$(document).ready(function(){
			$('.dataTables-example').DataTable({
				pageLength: 25,
				responsive: true,
				dom: '<"html5buttons"B>lTfgitp',
				buttons: [
				{ extend: 'copy'},
				{extend: 'csv'},
				{extend: 'excel', title: 'ExampleFile'},
				{extend: 'pdf', title: 'ExampleFile'},

				{extend: 'print',
				customize: function (win){
					$(win.document.body).addClass('white-bg');
					$(win.document.body).css('font-size', '10px');

					$(win.document.body).find('table')
					.addClass('compact')
					.css('font-size', 'inherit');
				}
			}
			]

		});

		});

	</script>


	<?php 
	if (@$mensaje == 'success') {
		echo '
		<script>
		swal({
			title: "¡Buen trabajo!",
			text: "'.$texto.'",
			type: "success"
			});
			</script>';
		}elseif (@$mensaje == 'error') {
			echo '
			<script>
			swal({
				title: "¡ALGO PASO!",
				text: "'.$texto.'",
				type: "error"
				});
				</script>';
			}elseif (@$mensaje == '1') {
				echo '
				<script>
				swal({
					title: "¡Buen trabajo!",
					text: "La orden se modifico con exito.!",
					type: "success"
					});
					</script>';
				}
				?>
			</body>
			</html>
